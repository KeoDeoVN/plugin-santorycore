package mk.plugin.santory.gui;

import org.bukkit.entity.Player;

public interface PlaceExecutor {
	
	public void execute(Player player, int slot, GUIStatus status);
	
}

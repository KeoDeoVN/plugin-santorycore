package mk.plugin.santory.skill;

import java.util.Map;

public interface SkillExecutor {
	
	public void start(Map<String, Object> map);
	
}
